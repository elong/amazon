$:.unshift './lib'
namespace :datasource do
  desc '下载亚马逊sitemap '
  task download_sitemap: :environment do
    require 'amazon/sitemap_downloader'
    AmazonSitemap.where("downloaded_at > ?", Time.now + 7.days).all.each do |as|
      Amazon::SitemapDownloader.new(as.url).download
      as.downloaded_at = Time.now
      as.save
    end
  end

  desc '下载亚马逊index'
  task download_index: :environment do
    require 'amazon/index_downloader'
    Amazon::IndexDownloader.new('http://www.amazon.com/sitemaps.f3053414d236e84.SitemapIndex_0.xml.gz
').download
  end
end
