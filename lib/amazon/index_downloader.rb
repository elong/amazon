require 'nokogiri'
require 'open-uri'
module Amazon
  class IndexDownloader
    def initialize(sitemap_index)
      @sitemap_index = ::Nokogiri::XML(open(sitemap_index))
    end

    def download
      list.each do |sitemap|
        as = ::AmazonSitemap.new
        as.url = sitemap
        as.save rescue false
      end
    end

    private
    def list
      @sitemap_index.search('sitemap/loc').map{|loc|loc.text}
    end
  end
end