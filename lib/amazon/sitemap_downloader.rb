require 'nokogiri'
require 'open-uri'
require 'uri'
module Amazon
  class SitemapDownloader
    def initialize(url)
      @sitemap = Nokogiri::XML(open(url))
    end

    def download
      list.each do |url|
        item = ::AmazonItem.new
        item.url = url
        item.name = URI(url).path.split('/')[1]
        begin
          item.save
        rescue ActiveRecord::RecordNotUnique => e
          warn e.to_s
        rescue ActiveRecord::StatementInvalid => e
          warn e.to_s
        end
      end
    end

    private

    def list
      @sitemap.search('url/loc').map{|loc|loc.text}
    end
  end
end
