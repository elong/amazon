# encoding utf-8
gem 'amazon-ecs','=2.2.5'
require 'amazon/ecs'

module Amazon
  class Ecs
    @@options = {
      :version => '2013-08-01',
      :service => 'AWSECommerceService',
      :response_group => 'Large',
      :country => 'us'
    }
  end
end

def browse_node_lookup(browse_node_id,access_key,secret_key,associate_tag)
  opts = {:response_group => 'BrowseNodeInfo',:AWS_access_key_id => access_key,:AWS_secret_key => secret_key,:associate_tag => associate_tag}
  result = Amazon::Ecs.browse_node_lookup('836313051',opts)
  if result.has_error?
    raise result.error
  end
  return nil if result.first_item.nil?
  result.doc #返回api xml
end

