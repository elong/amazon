class CreateAmazonSitemaps < ActiveRecord::Migration
  def change
    create_table :amazon_sitemaps do |t|
      t.string :url

      t.timestamps
    end
    add_index :amazon_sitemaps, :url,unique: true
  end
end
